<?php


include_once ("../../../"."vendor/autoload.php");
use \App\BITM\SEIP108367\Phonebook;
use App\BITM\SEIP108367\Utility\Utility;

$phonebook= new Phonebook();
$b=$phonebook->edit($_GET['id']);
//var_dump($b);
//die();
//Utility::dd($books);
?>

<html>
    <head>
        <title>phone book</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
         label { 
    display: inline-block; 
    width: 150px;
    text-align: right;
    margin-right:10px;  //How far the label element and input box
    margin-top:50px;
         }
    
h1 {
   
    text-align: center;
}
p {
   
    text-align: center;
}

        
        </style>
    </head>
    <body>
        <H1>Edit an Item</H1>
        <div class="container">
            <form action="update.php" method="post" class="form-inline">
                <div class="col-md-6">
                    <div><br>
                        <input type="hidden" class="form-control" 
                               name="id"
                               id="id" 
                               value="<?php echo $b->id;?>">
                        
                        <p><label for="name">Name:  </label>
                        <input type="text" name="name" value="<?php echo $b->name;?>"></p>
                               
                               
                    </div>
                   
                    <div>
                        <p><label for="phonenumber">Phone Number: </label>
                        <input type="number" name="phonenumber" value="<?php echo $b->phonenumber;?>"></p>
                        
                    </div>
                    
                     <div>
                        <p><label for="address">Address: </label>
                        <input type="text" name="address" value="<?php echo $b->address;?>"></p>
                        <br>
                        <p><button type="submit" class="btn btn-primary">Update</button></p>
                    </div>


            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
    </body>
</html>
