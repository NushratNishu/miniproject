<?php



include_once ("../../../"."vendor/autoload.php");


use App\BITM\SEIP108367\Phonebook;

$phonebook=new Phonebook();
$phonebooks=$phonebook->index();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title>Phone book</title>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                 
                <!-- Bootstrap -->
                <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                    <style>
                    h1 {
                        text-align: center;
                    }
                    p {
                        text-align: center;
                    }
                    </style>
                       </head>

                    <body>
                         <h1>List Of Phonebook</h1>
                         
                         
                      <p><button type="button" class="btn btn-info"><a href="create.php">Add New</a></button></p>
                       <div align="center">
                              <span>Search / Filter </span> 
                            <span id="utility">Download as <a href="pdf.php">PDF</a> | <a href="01simple-download-xlsx.php">XL</a>  <a href="create.php">Create New</a></span>
                          </div>
                         
                        <div class="container">
                            <table class="table table-bordered" class="table table-responsive" style="width:1000%">
                                <br>  <thead>
                                        <tr>
                                            <td><b>Sl.</b></td>
                                            <td><b>Name</b></td>		
                                            <td><b>Phone Number</b></td>
                                            <td><b>Address</b></td>
                                            <td colspan="3"><center><b>Action</b></center></td>
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        <?php 
                                        foreach ($phonebooks as $phonebook):
                                            ?>
                                        <tr>
                                            <td><?php echo $phonebook['id'];?></td>
                                            <td><?php echo $phonebook['name'];?></td>		
                                            <td><?php echo $phonebook['phonenumber'];?></td>
                                            <td><?php echo $phonebook['address'];?></td>
                                            <td colspan="3"><center><button type="submit" class="btn btn-success"><a href ="view.php?id=<?php echo $phonebook['id']?>">View</a></button>
                                                    <button type="submit" class="btn btn-primary"><a href ="edit.php?id=<?php echo $phonebook['id']?>">Edit</a></button>
                                                    <button type="submit" class="btn btn-danger"><a href ="delete.php?id=<?php echo $phonebook['id']?>">Delete</a></button></center></td>
                                        </tr>
                                        <?php 
                                        endforeach;
                                        ?>
                                      </tbody>
                                    <tfoot></tfoot>
                            </table>
                            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                           <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                            <!-- Include all compiled plugins (below), or include individual files as needed -->
                            <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
                        </div>
                    </body>
                    </html>


