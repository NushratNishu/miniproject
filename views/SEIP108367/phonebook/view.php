<?php

include_once ("../../../"."vendor/autoload.php");
use \App\BITM\SEIP108367\Phonebook;
use App\BITM\SEIP108367\Utility\Utility;
//print_r($_REQUEST);
//die();

$phonebook=new Phonebook();
$b =$phonebook->view($_GET['id']);
//Utility::dd($b);
?>



<html>
    <head>
        <title>Phone Book</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1><?php echo $b->name;?></h1>
        <dl>
             <dt>ID</dt>
            <dd><?php echo $b->id;?></dd> 
            <dt>Phone Number</dt>
            <dd><?php echo $b->phonenumber;?></dd>
            <dt>Address</dt>
            <dd><?php echo $b->address;?></dd>
        </dl>
    </body>
</html>
