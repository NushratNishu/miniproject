<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Sign up</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        
        
  <div class="container">
    <form class="form-horizontal" id="registration" method='post' action='signin.php'>
		<fieldset>
			<legend>Registration Form</legend>
			<div class="control-group">
				<label class="control-label">Username:</label>
				<div class="controls">
					<input type="text" id="username" name="user_name">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Email</label>
				<div class="controls">
					<input type="text" id="email" name="email">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Password:</label>
				<div class="controls">
					<input type="text" id="password" name="password2">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Gender</label>
				<div class="controls">
					<input type="radio" name="gender" value="male" checked> Male<br>
                                        <input type="radio" name="gender" value="female"> Female<br>
				</div>
			</div>
                        <br>
			<div class="control-group">
				<label class="control-label"></label>
				<div class="controls">
					<button type="submit" class="btn btn-success" >Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>

    </body>
</html>
